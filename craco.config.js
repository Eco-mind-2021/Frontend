const path = require('path')

const resolvePath = (p) => path.resolve(__dirname, p)

const devServerConfig = (devServerConfig) => ({
    ...devServerConfig,
    proxy: {
        '/api/v1': {
            target: process.env['REACT_APP_AUTH_BASE_URL'],
            changeOrigin: true,
            ws: false,
            pathRewrite: {
                '^/api/v1': '/api/v1',
            },
            secure: false,
        },
    },
})

module.exports = {
    style: {
        postcss: {
            plugins: [require('tailwindcss'), require('autoprefixer')],
        },
    },
    webpack: {
        alias: {
            '@': resolvePath('./src'),
            '@assets': resolvePath('./src/assets'),
            '@components': resolvePath('./src/components'),
            '@services': resolvePath('./src/services'),
            '@utils': resolvePath('./src/utils'),
            '@hooks': resolvePath('./src/hooks'),
            '@views': resolvePath('./src/views'),
            '@styles': resolvePath('./src/styles'),
        },
    },
    devServer: devServerConfig,
}
