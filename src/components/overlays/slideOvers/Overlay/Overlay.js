import React from 'react'
import {Portal} from '@components/elements'
import {OverlayBody} from '@components/overlays/slideOvers/Overlay/OverlayBody'

const Overlay = ({children, ...props}) => {
    return <OverlayBody {...props}>{children}</OverlayBody>
}

export {Overlay}
