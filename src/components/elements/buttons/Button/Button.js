import React from 'react'
import * as PropTypes from 'prop-types'
import cn from 'classnames'

const Button = ({
    className = '',
    color = 'primary',
    size = 'base',
    children,
    onClick,
    startIcon,
    endIcon,
    type = 'button',
    ...rest
}) => {
    return (
        <button
            {...rest}
            type={type}
            className={cn(
                className,
                'rounded-full inline-flex items-center border justify-center font-medium shadow-sm focus:outline-none disabled:opacity-50 focus:bg-gray-800 transition-all',
                size === 'sm' && 'px-2.5 py-1.5 text-xs',
                size === 'md' && 'px-3 py-2 text-sm font-medium leading-4',
                size === 'base' && 'px-4 py-2 text-sm font-medium',
                size === 'lg' && 'px-4 py-2 text-base font-medium',
                size === 'xl' && 'px-6 py-3 text-base font-medium',
                color === 'primary' &&
                    'border-transparent text-white bg-black hover:bg-gray-800 dark:bg-blue-400 dark:hover:bg-blue-300',
                color === 'secondary' &&
                    'border-transparent text-blue-600 bg-blue-100 hover:bg-blue-200',
                color === 'white' &&
                    'border-gray-300 dark:border-gray-600 text-gray-700 bg-white dark:bg-gray-700 dark:hover:bg-gray-600 dark:text-gray-200 hover:bg-gray-50'
            )}
            onClick={onClick}
        >
            {startIcon && (
                <div className='-ml-0.5 mr-2 h-4 w-4'>{startIcon}</div>
            )}
            {children}
            {endIcon && (
                <div className='ml-2 -mr-1 h-5 w-5' aria-hidden='true'>
                    {endIcon}
                </div>
            )}
        </button>
    )
}

Button.propTypes = {
    className: PropTypes.string,
    rounded: PropTypes.bool,
    color: PropTypes.oneOf(['primary', 'secondary', 'white']),
    size: PropTypes.oneOf(['sm', 'md', 'base', 'lg', 'xl']),
    onClick: PropTypes.func,
    startIcon: PropTypes.node,
    endIcon: PropTypes.node,
}
export {Button}
