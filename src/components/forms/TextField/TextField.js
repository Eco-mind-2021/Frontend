import React, {forwardRef, useRef} from 'react'
import {nanoid} from 'nanoid'
import cn from 'classnames'

const TextField = forwardRef(
    (
        {
            label,
            helperText,
            value,
            type = 'text',
            error,
            onChange,
            onBlur,
            onFocus,
            className,
            fieldClassName,
            inputClassName,
            labelClassName,
            helperClassName,
            leftArea,
            rightArea,
            topArea,
            disabled,
            ...rest
        },
        ref
    ) => {
        const id = useRef(nanoid(5))
        return (
            <div className={className}>
                {!!(label || topArea) && (
                    <div className='flex justify-between'>
                        {!!label && (
                            <label
                                htmlFor={id.current}
                                className={cn(
                                    'block text-sm font-medium text-gray-700 dark:text-gray-300',
                                    labelClassName,
                                    {
                                        'opacity-50': disabled,
                                    }
                                )}
                            >
                                {label}
                            </label>
                        )}
                        {topArea}
                    </div>
                )}
                <div
                    className={cn(
                        'mt-1 relative flex items-center',
                        fieldClassName
                    )}
                >
                    {leftArea && (
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            {leftArea}
                        </div>
                    )}
                    <input
                        ref={ref}
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                        onFocus={onFocus}
                        type={type}
                        disabled={disabled}
                        id={id.current}
                        className={cn(
                            'dark:bg-gray-900 shadow-sm disabled:opacity-50 focus:ring-blue-400 dark:focus:ring-blue-300 focus:border-blue-400 dark:focus:border-blue-300 block w-full sm:text-sm border-gray-300 dark:border-gray-600 rounded-md dark:text-gray-100',
                            {
                                'border-red-500 dark:border-red-300': error,
                            },
                            inputClassName
                        )}
                        {...rest}
                    />
                    {rightArea && (
                        <div className='absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none'>
                            {rightArea}
                        </div>
                    )}
                </div>
                {!!helperText && (
                    <p
                        className={cn(
                            'mt-2 text-sm text-gray-500 dark:text-gray-400',
                            {
                                'text-red-500 dark:text-red-300': error,
                            },
                            helperClassName
                        )}
                    >
                        {helperText}
                    </p>
                )}
            </div>
        )
    }
)

export {TextField}
