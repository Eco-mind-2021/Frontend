import {Switch} from '@headlessui/react'
import {Toggle} from './Toggle'

const ToggleLabeled = ({checked, onChange, label, description, right}) => {
    return (
        <Switch.Group as='div' className='flex items-center justify-between'>
            {!!right && <Toggle checked={checked} onChange={onChange} />}
            <span className='flex-grow flex flex-col'>
                {!!label && (
                    <Switch.Label
                        as='span'
                        className='text-sm font-medium text-gray-900 dark:text-gray-100'
                        passive
                    >
                        {label}
                    </Switch.Label>
                )}
                {!!description && (
                    <Switch.Description
                        as='span'
                        className='text-sm text-gray-500 dark:text-gray-400'
                    >
                        {description}
                    </Switch.Description>
                )}
            </span>
            {!right && <Toggle checked={checked} onChange={onChange} />}
        </Switch.Group>
    )
}

export {ToggleLabeled}
