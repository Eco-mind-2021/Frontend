import React from 'react'
import {RadioGroup} from '@headlessui/react'
import classNames from 'classnames'

const RadioGroupHorizontal = ({
    children,
    options = [],
    selected,
    onChange,
    className,
    optionClassName,
}) => {
    return (
        <RadioGroup
            value={selected}
            onChange={onChange}
            className={classNames(
                'w-full space-x-4 flex flex-row justify-between',
                className
            )}
        >
            {!!options.length &&
                options.map((option, index) => (
                    <RadioGroup.Option
                        key={index}
                        value={option}
                        className={({checked}) =>
                            classNames(
                                'relative flex-1 block rounded-xl px-6 py-4 cursor-pointer hover:bg-gray-800 focus:outline-none transition-all',
                                checked
                                    ? 'bg-[#5aad73] hover:bg-[#67c282]'
                                    : 'bg-black',
                                optionClassName
                            )
                        }
                    >
                        {({checked, active}) =>
                            children({option, checked, active})
                        }
                    </RadioGroup.Option>
                ))}
        </RadioGroup>
    )
}

export {RadioGroupHorizontal}
