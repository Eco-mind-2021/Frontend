import {Fragment} from 'react'
import {Listbox, Transition} from '@headlessui/react'
import classNames from 'classnames'

const Select = ({
    selected,
    onChange,
    options,
    label,
    renderButton,
    renderOption,
}) => {
    return (
        <Listbox value={selected} onChange={onChange}>
            {({open}) => (
                <>
                    <Listbox.Label className='block text-sm font-medium text-gray-700'>
                        {label}
                    </Listbox.Label>
                    <div className='mt-1 relative'>
                        <Listbox.Button className='bg-white relative w-full border border-gray-300 rounded-full shadow-sm pl-5 pr-10 py-4 text-left cursor-pointer sm:text-sm'>
                            {renderButton(selected)}
                        </Listbox.Button>

                        <Transition
                            show={open}
                            as={Fragment}
                            leave='transition ease-in duration-100'
                            leaveFrom='opacity-100'
                            leaveTo='opacity-0'
                        >
                            <Listbox.Options className='absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-3xl py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm'>
                                {options.map((option, index) => (
                                    <Listbox.Option
                                        key={index}
                                        className={({active}) =>
                                            classNames(
                                                active
                                                    ? 'text-white bg-[#5aad73]'
                                                    : 'text-gray-900',
                                                'cursor-pointer select-none relative py-2 pl-3 pr-9'
                                            )
                                        }
                                        value={option}
                                    >
                                        {({selected, active}) =>
                                            renderOption({
                                                option,
                                                selected,
                                                active,
                                            })
                                        }
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </>
            )}
        </Listbox>
    )
}

export {Select}
