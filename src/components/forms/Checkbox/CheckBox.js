import React, {forwardRef, useRef} from 'react'
import cn from 'classnames'
import {nanoid} from 'nanoid'
export const CheckBox = forwardRef(
    (
        {
            className,
            disabled,
            name = 'comments',
            label = '',
            description,
            checked,
            onChange,
            right,
            helperText,
            error,
            helperClassName,

            ...rest
        },
        ref
    ) => {
        const id = useRef(nanoid(5))
        const checkBox = (
            <div className='flex items-center h-5'>
                <input
                    ref={ref}
                    id={id.current}
                    aria-describedby='comments-description'
                    name={name}
                    type='checkbox'
                    checked={checked}
                    onChange={onChange}
                    disabled={disabled}
                    className={cn(
                        className,
                        'focus:ring-blue-400 dark:focus:ring-blue-300 disabled:opacity-50 h-4 w-4 text-blue-500 dark:text-blue-400 border-gray-300 dark:border-gray-500 rounded',
                        {
                            'border-red-500 dark:border-red-300': error,
                        }
                    )}
                    {...rest}
                />
            </div>
        )

        return (
            <div className='relative flex flex-col items-start '>
                <div className={'flex items-start'}>
                    {!right && checkBox}
                    <div
                        className={cn('text-sm', {
                            'ml-3': !right,
                            'mr-3': right,
                            'opacity-50': disabled,
                        })}
                    >
                        <label
                            htmlFor={id.current}
                            className='font-medium text-gray-700 dark:text-gray-300'
                        >
                            {label}
                        </label>
                        {description && (
                            <p
                                id={`${id.current}-description`}
                                className='text-gray-500 dark:text-gray-500'
                            >
                                {description}
                            </p>
                        )}
                    </div>
                    {right && checkBox}
                </div>
                {!!helperText && (
                    <p
                        className={cn(
                            'mt-2 text-sm text-gray-500 dark:text-gray-400',
                            {
                                'text-red-500 dark:text-red-300': error,
                            },
                            helperClassName
                        )}
                    >
                        {helperText}
                    </p>
                )}
            </div>
        )
    }
)
