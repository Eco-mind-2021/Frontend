import cn from 'classnames'

const Card = ({children, className, innerClassName}) => {
    return (
        <div
            className={cn(
                'bg-white dark:bg-gray-700 overflow-hidden shadow rounded-2xl',
                className
            )}
        >
            <div className={cn('px-4 py-5 sm:p-6 h-full', innerClassName)}>
                {children}
            </div>
        </div>
    )
}

export {Card}
