/**
 * Enum for common colors.
 * @readonly
 * @enum {string}
 */

export const AppPath = Object.freeze({
    home: '/',
    results: '/results',
    upload: '/upload',
})

export const FilterUrl = Object.freeze({
    search: '/search',
})

export const DatasetUrl = Object.freeze({
    index: '/Index',
})
