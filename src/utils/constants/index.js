import {AUTH_CLIENT_ID, AUTH_CLIENT_SECRET} from '@utils/constants/envConstants'

export const COMMON_PROPS = {
    client_id: process.env[AUTH_CLIENT_ID],
    client_secret: process.env[AUTH_CLIENT_SECRET],
}
