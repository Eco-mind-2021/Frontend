function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

const toCamel = (s) =>
    s.replace(/([-_][a-z])/gi, ($1) =>
        $1.toUpperCase().replace('-', '').replace('_', '')
    )

const keysToCamel = (o) => {
    const isObject = (o) =>
        o === Object(o) && !isArray(o) && typeof o !== 'function'
    const isArray = function (a) {
        return Array.isArray(a)
    }
    if (isObject(o)) {
        const n = {}

        Object.keys(o).forEach((k) => {
            n[toCamel(k)] = keysToCamel(o[k])
        })

        return n
    } else if (isArray(o)) {
        return o.map((i) => keysToCamel(i))
    }

    return o
}

const keysToSnake = (o) => {
    const isObject = (o) =>
        o === Object(o) && !isArray(o) && typeof o !== 'function'
    const isArray = function (a) {
        return Array.isArray(a)
    }
    if (isObject(o)) {
        const n = {}

        Object.keys(o).forEach((k) => {
            n[toSnake(k)] = keysToSnake(o[k])
        })

        return n
    } else if (isArray(o)) {
        return o.map((i) => keysToSnake(i))
    }

    return o
}
const toSnake = (string) =>
    string
        .replace(/[\w]([A-Z])/g, function (m) {
            return m[0] + '_' + m[1]
        })
        .toLowerCase()

const titleCase = (str) => {
    var splitStr = str.toLowerCase().split(' ')
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] =
            splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1)
    }
    return splitStr.join(' ')
}

export {classNames, keysToCamel, toCamel, keysToSnake, toSnake, titleCase}
