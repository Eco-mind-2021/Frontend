import React, {lazy} from 'react'
import {Route, Switch} from 'react-router-dom'
import {AppPath} from './routes-enums'
import MainLayout from '@views/layouts/MainLayout'

export const ROUTES = [
    {
        path: AppPath.home,
        exact: true,
        component: lazy(() =>
            import(/* webpackChunkName: "home" */ './views/home')
        ),
        protected: false,
    },
    {
        path: AppPath.results,
        exact: true,
        component: lazy(() =>
            import(/* webpackChunkName: "home" */ './views/results')
        ),
        protected: false,
    },
    {
        path: AppPath.upload,
        exact: true,
        component: lazy(() =>
            import(/* webpackChunkName: "home" */ './views/upload')
        ),
        protected: false,
    },
]

const RouteComponent = (route) => {
    return (
        <Route
            path={route.path}
            exact={route.exact}
            render={(props) => (
                <MainLayout>
                    {(layoutProps) => (
                        <route.component {...layoutProps} {...props} />
                    )}
                </MainLayout>
            )}
        />
    )
}

export const RenderRoutes = ({routes}) => {
    return (
        <Switch>
            {routes.map((route) => (
                <RouteComponent key={route.path} {...route} />
            ))}
        </Switch>
    )
}
