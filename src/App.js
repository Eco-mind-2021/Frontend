import React from 'react'
import {observer} from 'mobx-react'
import {RenderRoutes, ROUTES} from './routes'

const App = observer(() => {

    return <RenderRoutes routes={ROUTES} />
})

export default App
