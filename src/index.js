import React from 'react'
import ReactDOM from 'react-dom'
import '@styles/index.css'
import App from './App'
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'mobx-react'
import reportWebVitals from './reportWebVitals'
import RootStore from './store/RootStore'
import SimpleReactLightbox from 'simple-react-lightbox'

const rootStore = new RootStore()

const render = (Component) => {
    return ReactDOM.render(
        <Provider {...rootStore}>
            <SimpleReactLightbox>
                <BrowserRouter>
                    <Component />
                </BrowserRouter>
            </SimpleReactLightbox>
        </Provider>,
        document.getElementById('root')
    )
}

render(App)

if (module.hot) {
    module.hot.accept('./App', (_) => {
        const NextApp = require('./App').default
        render(NextApp)
    })
}
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
