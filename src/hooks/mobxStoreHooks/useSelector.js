import React from 'react'
import {useStore} from '@hooks'

export function useSelector(storeCb) {
    const store = useStore()
    if (typeof storeCb === 'function') return storeCb(store)
    return store
}
