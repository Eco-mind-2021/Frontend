import url from '@services/url'

const baseURL = `${url}/api/v1`

const baseConfig = {
    baseURL: baseURL,
    headers: {
        'Content-Type': 'application/json',
    },
}

export {baseConfig, baseURL}
