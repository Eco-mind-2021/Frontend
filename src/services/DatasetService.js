import {Api} from '@services/apiService'
import {DatasetUrl} from '@/routes-enums'
import {baseConfig} from '@services/apiCongfigs'

const datasetInstance = new Api(baseConfig)

class DatasetService {
    static postDataset = async (file) => {
        let formData = new FormData()
        formData.append('file', file, file.name)

        const config = {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        }
        return await datasetInstance.postFile(
            DatasetUrl.index,
            formData,
            config,
            () => {}
        )
    }
}

export default DatasetService
