let serverDomain = ''
if (process.env.REACT_APP_API_URL && process.env.REACT_APP_API_URL.length)
    serverDomain = process.env.REACT_APP_API_URL
else {
    serverDomain = 'http://eco-mind.ru:90'
}

const url = serverDomain

export default url
