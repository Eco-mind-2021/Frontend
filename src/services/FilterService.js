import {Api} from '@services/apiService'
import {FilterUrl} from '@/routes-enums'
import {baseConfig} from '@services/apiCongfigs'

const filterInstance = new Api(baseConfig)

class FilterService {
    static getResults = async (payload) => {
        return await filterInstance.get(FilterUrl.search, payload)
    }
}

export default FilterService
