import axiosLib from 'axios'
import {keysToCamel} from '@utils/helpers'

class Api {
    service

    constructor(config) {
        let service = axiosLib.create(config)
        service.interceptors.response.use(this.handleSuccess, this.handleError)
        this.service = service
    }

    async get(url, query, config) {
        let _url = url
        if (query) {
            let queryParams = new URLSearchParams(query)
            _url = _url + `?${queryParams}`
        }
        const {data} = await this.service.get(_url, config)
        return keysToCamel(data)
    }

    async post(url, body, config) {
        const {data} = await this.service.post(url, body, config)
        return keysToCamel(data)
    }

    async postFile(url, file, config, callBack, params) {
        const newConfig = {
            ...this._getProgress(callBack),
            ...config,
        }

        const {data} = await this.service.post(url, file, newConfig)
        return keysToCamel(data)
    }

    async put(url, body, config) {
        const {data} = await this.service.put(url, body, config)
        return keysToCamel(data)
    }

    async patch(url, body, config) {
        const {data} = await this.service.patch(url, body, config)
        return keysToCamel(data)
    }

    async delete(url, body, config) {
        const {data} = await this.service.delete(url, body, config)
        return keysToCamel(data)
    }

    handleSuccess(response) {
        return response
    }

    handleError(error) {
        return Promise.reject(error)
    }

    _getProgress(callback) {
        return {
            onUploadProgress: (progressEvent) => {
                const percentCompleted = Math.round(
                    (progressEvent.loaded * 100) / progressEvent.total
                )
                callback(percentCompleted)
            },
        }
    }
}

export {Api}
