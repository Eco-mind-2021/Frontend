import React, {useRef, useEffect} from 'react'
import {Link} from 'react-router-dom'

import Filter from '@views/common/Filter/Filter'
import PhotoSearch from '@views/common/PhotoSearch/PhotoSearch'
import ResultsList from './ResultsList/ResultsList'

import {ArrowLeftIcon} from '@heroicons/react/outline'

const Results = () => {
    const stickyEl = useRef()

    useEffect(() => {
        const initialTop = stickyEl.current.offsetTop
        const makeSticky = (e) => {
            if (stickyEl.current.offsetHeight < window.innerHeight) {
                stickyEl.current.classList.add('sticky', 'top-8')
                stickyEl.current.style.width = ''
            } else {
                stickyEl.current.classList.remove('sticky', 'top-8')
                if (
                    window.scrollY >
                    stickyEl.current.offsetHeight +
                        32 +
                        initialTop -
                        window.innerHeight
                ) {
                    const width = stickyEl.current.offsetWidth
                    stickyEl.current.classList.add('fixed', 'bottom-0')
                    stickyEl.current.style.width = `${width}px`
                    stickyEl.current.parentNode.style.height = `${
                        stickyEl.current.offsetHeight + 32
                    }px`
                } else {
                    stickyEl.current.classList.remove('fixed', 'bottom-0')
                    stickyEl.current.style.width = ''
                }
            }
        }
        window.addEventListener('scroll', makeSticky)
        return () => {
            window.removeEventListener('scroll', makeSticky)
        }
    }, [])

    return (
        <div className={'max-w-screen-2xl mx-auto px-4 md:px-8'}>
            <div className={'flex items-stretch h-full py-8 lg:space-x-16'}>
                <aside className={'hidden lg:block lg:w-1/3'}>
                    <div ref={stickyEl} className={'mb-8 space-y-6'}>
                        <Filter />
                        <PhotoSearch />
                    </div>
                </aside>
                <main className={'w-full lg:w-2/3'}>
                    <Link to={'/'} className={'flex items-center mb-6'}>
                        <ArrowLeftIcon className={'h-6 mr-2'} />
                        <span className={'text-xl font-medium'}>Назад</span>
                    </Link>
                    <ResultsList />
                </main>
            </div>
        </div>
    )
}

export default Results
