import React, {useState} from 'react'
import {Map, Placemark} from 'react-yandex-maps'
import {SRLWrapper} from 'simple-react-lightbox'
import classNames from 'classnames'

import {Card} from '@components/layout'

import {titleCase} from '@utils/helpers'

import BREEDS from '../breeds'

const COLORS = {
    0: null,
    1: 'Тёмный',
    2: 'Светлый',
    3: 'Цветной',
}

const TAILS = {
    0: null,
    1: 'Короткий',
    2: 'Длинный',
}

const ResultsListItem = ({
    address,
    image,
    color,
    owner,
    tail,
    cameraId,
    cameraTime,
    breed,
}) => {
    const [mapState, setMapState] = useState({center: [55.75, 37.57], zoom: 17})

    const geocode = (ymaps, addr) => {
        ymaps.geocode(addr).then((result) => {
            setMapState({
                center: result.geoObjects.get(0).geometry.getCoordinates(),
                zoom: 17,
            })
        })
    }

    const getBreedName = (string) => {
        return titleCase(string.replace(/_/g, ' '))
    }

    const renderData = () => {
        return (
            <>
                <div>
                    <table className={'table-auto'}>
                        {!!color || !!tail ? (
                            <p className={'text-lg font-bold mb-2'}>Собака</p>
                        ) : (
                            <p className={'text-lg font-bold mb-2'}>
                                Нет собаки
                            </p>
                        )}
                        {!!breed?.length && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Порода:
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {breed.map((item, index) => (
                                        <p key={index}>
                                            {getBreedName(BREEDS[item])}
                                        </p>
                                    ))}
                                </td>
                            </tr>
                        )}
                        {!!color && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Цвет:
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {COLORS[String(color)]}
                                </td>
                            </tr>
                        )}
                        {!!tail && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Хвост:
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {TAILS[String(tail)]}
                                </td>
                            </tr>
                        )}
                        {!!String(owner).length && (!!color || !!tail) && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Хозяин:
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {owner ? 'Есть' : 'Нет'}
                                </td>
                            </tr>
                        )}
                    </table>
                </div>
                <div>
                    <table className={'table-auto'}>
                        {(!!cameraId?.length || !!address?.length) && (
                            <p className={'text-lg font-bold mb-2'}>Камера</p>
                        )}
                        {!!cameraId?.length && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    ID:{' '}
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {cameraId}
                                </td>
                            </tr>
                        )}
                        {!!address?.length && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Адрес:{' '}
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {address}
                                </td>
                            </tr>
                        )}
                        {!!cameraTime?.length && (
                            <tr>
                                <th
                                    className={
                                        'text-gray-500 font-bold text-left align-top'
                                    }
                                >
                                    Время:{' '}
                                </th>
                                <td className={'text-left align-top pl-3'}>
                                    {cameraTime}
                                </td>
                            </tr>
                        )}
                    </table>
                </div>
            </>
        )
    }

    return (
        <SRLWrapper options={{thumbnails: {showThumbnails: false}}}>
            <Card
                innerClassName={'py-0 px-0 sm:p-0 flex flex-col w-full'}
                className={'shadow-md'}
            >
                <div
                    className={classNames(
                        'flex items-stretch w-full',
                        !address ? 'flex-col md:flex-row' : ''
                    )}
                >
                    <div
                        className={classNames(
                            'relative block',
                            !address ? 'w-full md:w-1/2' : 'w-1/2 h-32 md:h-60'
                        )}
                    >
                        <a href={`//eco-mind.ru:90${image}`}>
                            <img
                                className={
                                    'absolute top-0 left-0 h-full w-full object-cover object-bottom bg-black cursor-pointer'
                                }
                                src={`//eco-mind.ru:90${image}`}
                                alt=''
                            />
                        </a>
                    </div>
                    {!!address && (
                        <Map
                            state={mapState}
                            onLoad={(ymaps) => geocode(ymaps, address)}
                            modules={['geocode']}
                            className={'h-32 md:h-60 w-1/2'}
                        >
                            {!mapState?.center ? null : (
                                <Placemark geometry={mapState.center} />
                            )}
                        </Map>
                    )}
                    {!address && (
                        <div className={'p-6 space-y-4'}>{renderData()}</div>
                    )}
                </div>
                {!!address && (
                    <div
                        className={
                            'p-6 grid grid-cols-1 space-y-4 md:grid-cols-2'
                        }
                    >
                        {renderData()}
                    </div>
                )}
            </Card>
        </SRLWrapper>
    )
}

export default ResultsListItem
