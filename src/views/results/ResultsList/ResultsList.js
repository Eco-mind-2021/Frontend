import React from 'react'
import {observer} from 'mobx-react'
import {useSelector} from '@hooks'

import {YMaps} from 'react-yandex-maps'

import {Button, Loader} from '@components/elements'

import ResultsListItem from './ResultsListItem'

import {ReactComponent as DogImage} from '../images/dog.svg'

const ResultsList = () => {
    const {photos, output, getMore, pending} = useSelector(
        (store) => store.resultsStore
    )

    return (
        <div className={'relative block z-0'}>
            <h1 className={'text-3xl sm:text-5xl font-black mb-8'}>
                Результаты поиска{' '}
                <span className={'text-gray-500'}>{photos.length}</span>
            </h1>
            <YMaps
                query={{
                    ns: 'use-load-option',
                    apikey: '778a94fc-7c6c-40fe-8f75-e78f388fd196',
                    load: ['geocode'],
                }}
            >
                <div className={'space-y-4'}>
                    {!!output?.length &&
                        output.map((photo, index) => {
                            return (
                                <ResultsListItem
                                    key={index}
                                    address={photo?.cameraAddress}
                                    cameraId={photo?.cameraId}
                                    cameraTime={photo?.cameraDatetime}
                                    color={photo?.color}
                                    tail={photo?.tail}
                                    owner={photo?.ownerClass}
                                    image={photo?.fileName}
                                    breed={photo?.breed}
                                />
                            )
                        })}
                    {photos?.length > 10 && photos.length !== output.length && (
                        <Button
                            size={'xl'}
                            className={'mx-auto'}
                            onClick={getMore}
                        >
                            Показать ещё
                        </Button>
                    )}
                    {!output?.length && <DogImage />}
                </div>
            </YMaps>

            {pending && (
                <div
                    className={
                        'absolute top-0 left-0 flex w-full h-full items-center justify-center bg-white opacity-75 z-[9999999]'
                    }
                >
                    <Loader />
                </div>
            )}
        </div>
    )
}

export default observer(ResultsList)
