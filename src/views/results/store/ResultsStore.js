import {action, makeObservable, observable} from 'mobx'

class ResultsStore {
    constructor(rootStore) {
        this.rootStore = rootStore
        makeObservable(this, {
            photos: observable,
            setPhotos: action,
            output: observable,
            pushOutput: action,
            setOutput: action,
            page: observable,
            getMore: action,
            pending: observable,
            setPending: action,
        })
    }

    photos = []

    output = []

    setOutput = (value = []) => {
        this.output = value
    }

    pushOutput = (array = []) => {
        this.output.push(...array)
    }

    page = 1

    setPage = (page = 1) => {
        this.page = page
    }

    getMore = () => {
        const start = this.page * 10
        this.pushOutput(this.photos.slice(start, start + 10))
        this.setPage(this.page + 1)
    }

    setPhotos = (value = []) => {
        this.photos = value
    }

    mergePhotosData = (source) => {
        this.setPhotos(source)
        this.setOutput(source.slice(0, 10))
        this.setPage(1)
        this.setPending(false)
    }

    pending = false

    setPending = (value = true) => {
        this.pending = value
    }
}

export default ResultsStore
