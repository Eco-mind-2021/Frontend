import React, {useState} from 'react'
import {useDropzone} from 'react-dropzone'

import DatasetService from '@services/DatasetService'

import {Button, Loader} from '@components/elements'

const Upload = () => {
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone()
    const [pending, setPending] = useState(false)
    const [success, setSuccess] = useState(false)
    const [data, setData] = useState('')

    const files = acceptedFiles.map((file) => (
        <li key={file.path}>
            {file.path} - {file.size} байт
        </li>
    ))

    const uploadFile = async () => {
        setPending(true)
        try {
            const data = await DatasetService.postDataset(acceptedFiles[0])
            setData(data)
            setPending(false)
            setSuccess(true)
        } catch (e) {
            console.log(e)
            setPending(false)
            setSuccess(false)
        }
    }

    return (
        <div className={'max-w-screen-2xl mx-auto px-8'}>
            <h1 className={'text-4xl font-bold pb-8'}>Загрузить фото</h1>
            <p className={'mb-6'}>
                Для загрузки через фронтенд датасет должен быть в ZIP-архиве,
                без вложенных папок, желательно не более 100МБ
            </p>
            {!!success && (
                <>
                    <div
                        className={
                            'bg-green-200 rounded-3xl p-8 flex items-center mb-8'
                        }
                    >
                        <p className={'text-green-600 font-medium text-xl'}>
                            Дата-сет успешно загружен
                        </p>
                    </div>
                    <div
                        className={
                            'bg-gray-200 rounded-3xl p-8 flex flex-col items-start mb-8'
                        }
                    >
                        <p className={'font-medium text-xl mb-6'}>Ответ</p>
                        <p className={'block'}>{data}</p>
                    </div>
                </>
            )}
            <div
                {...getRootProps({
                    className:
                        'dropzone rounded-3xl border-4 border-dashed p-8 cursor-pointer mb-8',
                })}
            >
                <input {...getInputProps()} />
                {!acceptedFiles.length ? (
                    <p>
                        Перенесите Zip-архив с фотографиями сюда, либо нажмите и
                        выберите файл
                    </p>
                ) : (
                    <>
                        <p>Файл для загрузки:</p>
                        <ul>{files}</ul>
                    </>
                )}
            </div>
            <Button
                size={'xl'}
                disabled={!acceptedFiles.length}
                onClick={uploadFile}
            >
                Загрузить датасет
            </Button>
            {!!pending && (
                <div
                    className={
                        'fixed top-0 left-0 flex w-full h-full items-center justify-center bg-white opacity-75 z-[9999999]'
                    }
                >
                    <Loader />
                </div>
            )}
        </div>
    )
}

export default Upload
