import React, {Suspense} from 'react'
import {observer} from 'mobx-react'

import {Loader} from '@components/elements'
import Header from './Header/Header'

const MainLayout = observer(({children}) => {
    return (
        <>
            <div>
                <Header />
                <Suspense
                    fallback={
                        <div
                            className={
                                'fixed top-0 left-0 flex w-full h-full items-center justify-center bg-white opacity-75 z-[9999999]'
                            }
                        >
                            <Loader />
                        </div>
                    }
                >
                    {children()}
                </Suspense>
            </div>
        </>
    )
})

export default MainLayout
