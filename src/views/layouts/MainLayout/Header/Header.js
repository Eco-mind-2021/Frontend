import React from 'react'
import {Link} from 'react-router-dom'

import {ReactComponent as Logo} from '@assets/logo.svg'
import {
    CameraIcon as CameraIconOutline,
    FolderAddIcon,
} from '@heroicons/react/outline'

const Header = () => {
    return (
        <header
            className={
                'max-w-screen-2xl mx-auto px-4 md:px-8 flex flex-col md:flex-row items-center justify-between py-4'
            }
        >
            <Link
                to={'/'}
                className={
                    'flex flex-col md:flex-row items-center whitespace-nowrap'
                }
            >
                <span className={'flex items-center -ml-8 md:ml-0'}>
                    <Logo
                        className={'stroke-current text-gray-800 h-16 mr-2'}
                    />
                    <span className={'text-2xl font-bold'}>Eco Mind</span>
                </span>
                <span className={'text-xs text-gray-500 md:ml-8'}>
                    «Лидеры цифровой <br />
                    трансформации 2021»
                </span>
            </Link>
            <div
                className={
                    'hidden md:flex flex-col md:flex-row items-center space-x-8 whitespace-nowrap'
                }
            >
                <Link
                    to={'/upload'}
                    className={
                        'flex items-center text-base text-gray-500 hover:text-[#5aad73] transition-all'
                    }
                >
                    <FolderAddIcon className={'h-6 mr-2 stroke-current'} />
                    <span>Загрузить фотоархив</span>
                </Link>
            </div>
        </header>
    )
}

export default Header
