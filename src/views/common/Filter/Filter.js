import React, {useEffect} from 'react'
import {observer} from 'mobx-react'
import {Link} from 'react-router-dom'
import classNames from 'classnames'

import {useSelector} from '@hooks'

import {Button} from '@components/elements'
import {Select} from '@components/forms'

import ColorSelector from './children/ColorSelector'
import TailSelector from './children/TailSelector'
import OwnerSelector from './children/OwnerSelector'

import {CheckIcon, SelectorIcon} from '@heroicons/react/solid'

const Filter = () => {
    const {
        color,
        setColor,
        tail,
        setTail,
        owner,
        setOwner,
        firstSearch,
        breed,
        setBreed,
        runSearch,
    } = useSelector((store) => store.filterStore)

    return (
        <div className={'w-full bg-gray-100 rounded-3xl p-6 space-y-6'}>
            <h2 className={'text-3xl sm:text-4xl font-bold mb-8'}>
                Параметры поиска
            </h2>
            <div className={'space-y-5'}>
                <ColorSelector
                    selected={color.selected}
                    options={color.options}
                    onChange={setColor}
                />
                <TailSelector
                    selected={tail.selected}
                    options={tail.options}
                    onChange={setTail}
                />
                <OwnerSelector
                    selected={owner.selected}
                    options={owner.options}
                    onChange={setOwner}
                />
                <div>
                    <h3 className={'text-xl font-medium mb-3'}>Порода</h3>
                    <Select
                        selected={breed.selected}
                        options={breed.options}
                        onChange={setBreed}
                        renderButton={(selected) => {
                            return (
                                <>
                                    <span className='block truncate'>
                                        {selected.name}
                                    </span>
                                    <span className='absolute inset-y-0 right-0 flex items-center pr-5 pointer-events-none'>
                                        <SelectorIcon
                                            className='h-5 w-5 text-gray-400'
                                            aria-hidden='true'
                                        />
                                    </span>
                                </>
                            )
                        }}
                        renderOption={({option, selected, active}) => (
                            <>
                                <span
                                    className={classNames(
                                        selected
                                            ? 'font-semibold'
                                            : 'font-normal',
                                        'block truncate'
                                    )}
                                >
                                    {option.name}
                                </span>

                                {selected ? (
                                    <span
                                        className={classNames(
                                            active
                                                ? 'text-white'
                                                : 'text-[#5aad73]',
                                            'absolute inset-y-0 right-0 flex items-center pr-4'
                                        )}
                                    >
                                        <CheckIcon
                                            className='h-5 w-5'
                                            aria-hidden='true'
                                        />
                                    </span>
                                ) : null}
                            </>
                        )}
                    />
                </div>
                <Link to={'/results'} className={'block'}>
                    <Button
                        size={'xl'}
                        className={'w-full'}
                        onClick={() => {
                            runSearch()
                            window.scrollTo({
                                top: 0,
                                behavior: 'smooth',
                            })
                        }}
                    >
                        {firstSearch ? 'Начать поиск' : 'Повторить поиск'}
                    </Button>
                </Link>
            </div>
        </div>
    )
}

export default observer(Filter)
