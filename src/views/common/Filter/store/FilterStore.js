import {action, computed, makeObservable, observable} from 'mobx'
import FilterService from '@services/FilterService'
import BREEDS from '@views/results/breeds'
import {titleCase} from '@utils/helpers'

class FilterStore {
    constructor(rootStore) {
        this.rootStore = rootStore
        makeObservable(this, {
            color: observable,
            setColor: action,
            tail: observable,
            setTail: action,
            owner: observable,
            setOwner: action,
            breed: observable,
            setBreed: action,
            filter: computed,
        })
        this.setInitialStore()
    }

    setInitialStore = () => {
        this.setColor(this.color.options[0])
        this.setTail(this.tail.options[0])
        this.setOwner(this.owner.options[0])
        this.setBreed(this.breed.options[0])

        this.setFirstSearch(true)
    }

    color = {
        options: [
            {
                label: 'Светлый',
                value: '2',
                image: 'withoutMan',
                className: 'text-[#e6be93]',
                checkedClassName: 'text-[#f7ddc1]',
            },
            {
                label: 'Тёмный',
                value: '1',
                image: 'withoutMan',
                className: 'text-[#8a5041]',
                checkedClassName: 'text-[#8c503f]',
            },
            {
                label: 'Цветной',
                value: '3',
                image: 'color',
                className: 'text-[#3093f0]',
                checkedClassName: 'text-[#107de3]',
            },
        ],
        selected: undefined,
    }

    setColor = (value) => {
        this.color.selected = value
    }

    tail = {
        options: [
            {
                label: 'Длинный',
                value: '2',
                image: 'longTail',
                className: 'text-gray-200',
                checkedClassName: 'text-gray-200',
            },
            {
                label: 'Короткий',
                value: '1',
                image: 'shortTail',
                className: 'text-[#e6be93]',
                checkedClassName: 'text-[#f7ddc1]',
            },
        ],
        selected: undefined,
    }

    setTail = (value) => {
        this.tail.selected = value
    }

    owner = {
        options: [
            {
                label: 'С хозяином',
                value: '1',
                image: 'withMan',
                className: 'text-[#e6be93] h-12 -mt-4',
                checkedClassName: 'text-[#f7ddc1] h-12 -mt-4',
            },
            {
                label: 'Без хозяина',
                value: '0',
                image: 'withoutManCrop',
                className: 'text-[#b5644e] h-8',
                checkedClassName: 'text-[#bf6047] h-8',
            },
        ],
        selected: undefined,
    }

    setOwner = (value) => {
        this.owner.selected = value
    }

    getBreedName = (string) => {
        return titleCase(string.replace(/_/g, ' '))
    }

    convertDictionaryToOptions = (dict) => {
        const keys = Object.keys(dict)
        const options = []
        for (let i = 0; i < keys.length; i++) {
            options.push({id: keys[i], name: this.getBreedName(dict[keys[i]])})
        }
        return options
    }

    breed = {
        options: [
            {id: 'any', name: 'Любая'},
            ...this.convertDictionaryToOptions(BREEDS),
        ],
        selected: undefined,
    }

    setBreed = (value) => {
        this.breed.selected = value
    }

    firstSearch = true

    setFirstSearch = (value = false) => {
        this.firstSearch = value
    }

    get filter() {
        const filter = {
            'Filter.Color': this.color.selected.value,
            'Filter.Tail': this.tail.selected.value,
            'Filter.OwnerClass': this.owner.selected.value,
        }
        if (this.breed.selected.id !== 'any')
            filter['Filter.Breed'] = this.breed.selected.id
        return filter
    }

    runSearch = async () => {
        try {
            this.rootStore.resultsStore.setPending(true)
            const results = await FilterService.getResults(this.filter)
            this.rootStore.resultsStore.mergePhotosData(results)
            if (this.firstSearch) this.setFirstSearch(false)
        } catch (e) {
            console.log(e)
            this.rootStore.resultsStore.setPending(false)
            this.rootStore.resultsStore.mergePhotosData([])
            if (this.firstSearch) this.setFirstSearch(false)
        }
    }
}

export default FilterStore
