import React from 'react'
import classNames from 'classnames'

import {RadioGroupHorizontal} from '@components/forms'

import {ReactComponent as WithMan} from '../images/withMan.svg'
import {ReactComponent as WithoutManCrop} from '../images/withoutManCrop.svg'

const DOG_ICONS = {
    withMan: WithMan,
    withoutManCrop: WithoutManCrop,
}

const OwnerSelector = ({selected, options, onChange}) => {
    return (
        <div>
            <h3 className={'text-xl font-medium mb-3'}>Человек рядом</h3>
            <RadioGroupHorizontal
                selected={selected}
                onChange={onChange}
                options={options}
                optionClassName={'pb-0'}
                className={
                    'flex-col xl:flex-row space-y-4 xl:space-y-0 space-x-0 xl:space-x-4'
                }
            >
                {({option, checked}) => (
                    <div className={'h-full flex flex-col justify-between'}>
                        <p className={'text-sm font-bold text-gray-100'}>
                            {option.label}
                        </p>
                        {(() => {
                            const IconComponent = DOG_ICONS[option.image]
                            return (
                                <IconComponent
                                    className={classNames(
                                        'fill-current self-end',
                                        checked
                                            ? option.checkedClassName
                                            : option.className
                                    )}
                                />
                            )
                        })()}
                    </div>
                )}
            </RadioGroupHorizontal>
        </div>
    )
}

export default OwnerSelector
