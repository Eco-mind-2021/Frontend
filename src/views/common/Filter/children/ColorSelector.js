import React from 'react'
import classNames from 'classnames'

import {RadioGroupHorizontal} from '@components/forms'

import {ReactComponent as WithoutMan} from '../images/withoutMan.svg'
import {ReactComponent as Color} from '../images/color.svg'

const DOG_ICONS = {
    withoutMan: WithoutMan,
    color: Color,
}

const ColorSelector = ({options, selected, onChange}) => {
    return (
        <div>
            <h3 className={'text-xl font-medium mb-3'}>Окрас</h3>
            <RadioGroupHorizontal
                selected={selected}
                onChange={onChange}
                options={options}
                className={
                    'flex-col xl:flex-row space-y-4 xl:space-y-0 space-x-0 xl:space-x-4'
                }
            >
                {({option, checked}) => (
                    <div
                        className={
                            'xl:h-full flex xl:flex-col justify-between xl:space-y-4'
                        }
                    >
                        <p className={'text-sm font-bold text-gray-100'}>
                            {option.label}
                        </p>
                        {(() => {
                            const IconComponent = DOG_ICONS[option.image]
                            return (
                                <IconComponent
                                    className={classNames(
                                        'fill-current h-16 max-w-full',
                                        checked
                                            ? option.checkedClassName
                                            : option.className
                                    )}
                                />
                            )
                        })()}
                    </div>
                )}
            </RadioGroupHorizontal>
        </div>
    )
}

export default ColorSelector
