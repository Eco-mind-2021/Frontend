import React from 'react'
import classNames from 'classnames'

import {RadioGroupHorizontal} from '@components/forms'

import {ReactComponent as LongTail} from '../images/longTail.svg'
import {ReactComponent as ShortTail} from '../images/shortTail.svg'

const DOG_ICONS = {
    longTail: LongTail,
    shortTail: ShortTail,
}

const TailSelector = ({selected, options, onChange}) => {
    return (
        <div>
            <h3 className={'text-xl font-medium mb-3'}>Длина хвоста</h3>
            <RadioGroupHorizontal
                selected={selected}
                onChange={onChange}
                options={options}
                optionClassName={'pr-0'}
                className={
                    'flex-col xl:flex-row space-y-4 xl:space-y-0 space-x-0 xl:space-x-4'
                }
            >
                {({option, checked}) => (
                    <div className={'flex justify-between'}>
                        <p className={'text-sm font-bold text-gray-100'}>
                            {option.label}
                        </p>
                        {(() => {
                            const IconComponent = DOG_ICONS[option.image]
                            return (
                                <IconComponent
                                    className={classNames(
                                        'fill-current h-16',
                                        checked
                                            ? option.checkedClassName
                                            : option.className
                                    )}
                                />
                            )
                        })()}
                    </div>
                )}
            </RadioGroupHorizontal>
        </div>
    )
}

export default TailSelector
