import React from 'react'
import {Button} from '@components/elements'

const PhotoSearch = () => {
    return (
        <div className={'w-full bg-yellow-200 rounded-3xl p-6 space-y-6'}>
            <h2 className={'text-3xl sm:text-4xl font-bold mb-8'}>
                Поиск по фото
            </h2>
            <Button className={'w-full'} size={'xl'} disabled>
                Скоро...
            </Button>
        </div>
    )
}

export default PhotoSearch
