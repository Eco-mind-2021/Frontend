import React from 'react'
import {ArrowLeftIcon} from '@heroicons/react/outline'

import {useResponsive} from '@hooks'

import Filter from '@views/common/Filter/Filter'
import PhotoSearch from '@views/common/PhotoSearch/PhotoSearch'

import {ReactComponent as RunningDogImage} from './images/runningDog.svg'

const Content = () => {
    const {lg} = useResponsive()
    return (
        <>
            <div className={'flex flex-col justify-center space-y-8'}>
                <div className={'flex flex-col mb-2 lg:mb-8'}>
                    <h1 className={'text-3xl sm:text-5xl font-bold mb-16'}>
                        Поможем найти <br />
                        потерявшегося Друга
                    </h1>
                    <RunningDogImage className={'h-98 self-end'} />
                    <div className={'flex items-center mt-8 lg:-mt-8'}>
                        <ArrowLeftIcon
                            className={'h-8 mr-6 -rotate-90 lg:rotate-0'}
                        />
                        <span className={'text-base md:text-lg font-medium'}>
                            Выберите параметры питомца <br />и начинайте поиск
                        </span>
                    </div>
                </div>
                {!lg && <Filter />}
                {!lg && <PhotoSearch />}
                <div
                    className={
                        'w-full h-48 bg-yellow-200 rounded-3xl p-6 md:p-8'
                    }
                >
                    <h2 className={'text-2xl sm:text-4xl font-bold mb-16'}>
                        Как это работает
                    </h2>
                </div>
                <div
                    className={'w-full h-48 bg-gray-100 rounded-3xl p-6 md:p-8'}
                >
                    <h2 className={'text-2xl sm:text-4xl font-bold mb-16'}>
                        Успехи сервиса
                    </h2>
                </div>
                <div
                    className={
                        'w-full h-48 bg-[#5aad73] rounded-3xl p-6 md:p-8'
                    }
                >
                    <h2
                        className={
                            'text-2xl sm:text-4xl font-bold mb-16 text-white'
                        }
                    >
                        Что мы можем ещё
                    </h2>
                </div>
                <div
                    className={'w-full h-48 bg-gray-100 rounded-3xl p-6 md:p-8'}
                >
                    <h2 className={'text-2xl sm:text-4xl font-bold mb-16'}>
                        Благодарности
                    </h2>
                </div>
            </div>
        </>
    )
}

export default Content
