import ThemeStore from './ThemeStore'
import FilterStore from '@views/common/Filter/store/FilterStore'
import ResultsStore from '@views/results/store/ResultsStore'

class rootStore {
    constructor() {
        this.themeStore = new ThemeStore(this)
        this.filterStore = new FilterStore(this)
        this.resultsStore = new ResultsStore(this)
    }
}

export default rootStore
